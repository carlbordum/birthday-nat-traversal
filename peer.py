"""
NAT traversal by exploiting the birthday problem[0]
===================================================

This small, experimental script is inspired by an idea presented in an
excellent blog post by David Anderson from Tailscale[1].

It will not work if both peers are behind Endpoint-Dependent Mapping (EDM) /
"hard" NATs. The easy side has to guess the correct port on the hard side.
This script achieves NAT traversal by simply binding to 256 ports on both sides
and sending to 256 random ports on the other side. This gives the easy side 64%
chance of creating a succesful "connection".

Apparently, most home routers are easy NATs, so this should work in most
friend-to-friend scenarios :-)

[0] https://en.wikipedia.org/wiki/Birthday_problem
[1] https://tailscale.com/blog/how-nat-traversal-works/
"""


import random
import select
import socket


def randrangen(start, stop, n):
    numbers = list(range(start, stop))
    random.shuffle(numbers)
    yield from numbers[:n]


ip = input("IP: ")
msg = input("msg: ").encode()


print("[+] Sending packets")
sockets = []
for port in randrangen(start=1024, stop=65536, n=256):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind(("0.0.0.0", port))
    s.sendto(msg, (ip, port))
    sockets.append(s)


print("[+] Waiting for response")
(s, *__), *___ = select.select(sockets, [], [])
data, addr = s.recvfrom(1024)
print("[+] Received response")
print(f"< {data.decode()}")
